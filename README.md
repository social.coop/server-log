# server-log

Log of upgrades, maintenance, and outages. If you're on the tech ops team and do work on the server, put it here to keep everyone informed.